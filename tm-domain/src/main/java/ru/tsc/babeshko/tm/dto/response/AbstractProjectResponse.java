package ru.tsc.babeshko.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.ProjectDto;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDto project;

    public AbstractProjectResponse(@Nullable final ProjectDto project) {
        this.project = project;
    }

}