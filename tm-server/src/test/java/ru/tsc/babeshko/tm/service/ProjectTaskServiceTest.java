package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.babeshko.tm.api.service.dto.*;
import ru.tsc.babeshko.tm.api.service.dto.ITaskDtoService;
import ru.tsc.babeshko.tm.api.service.dto.IUserDtoService;
import ru.tsc.babeshko.tm.configuration.ContextConfiguration;
import ru.tsc.babeshko.tm.dto.model.ProjectDto;
import ru.tsc.babeshko.tm.dto.model.TaskDto;
import ru.tsc.babeshko.tm.dto.model.UserDto;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    private String userId;

    private String projectId;

    private String taskId;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);
        projectService = context.getBean(IProjectDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        @NotNull final UserDto user = userService.create("user", "user");
        userId = user.getId();
        @NotNull final ProjectDto project = projectService.create(userId, "project");
        projectId = project.getId();
        @NotNull final TaskDto task = taskService.create(userId, "task");
        taskId = task.getId();
    }

    @After
    public void end() {
        taskService.clear(userId);
        projectService.clear(userId);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "not_task_id"));

        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @NotNull final TaskDto task = taskService.findOneById(taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, "not_task_id"));
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        @NotNull final TaskDto task = taskService.findOneById(taskId);
        Assert.assertNull(task.getProjectId());
    }

}